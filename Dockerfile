FROM alpine:3.16.0 AS builder

# Add source files
ADD https://github.com/samhocevar/rinetd/releases/download/v0.73/rinetd-0.73.tar.gz /

# Download compile dependencies
RUN apk add --no-cache build-base

# Compile and install
RUN set -xe && \
    mkdir /build/ && \
    tar -xf rinetd-0.73.tar.gz && \
    cd rinetd-0.73 && \
    sh ./configure --prefix=/build/ --disable-dependency-tracking --sysconfdir=/etc/rinetd && \
    make install

# Clean up
RUN rm -rf rinetd-0.73.tar.gz && \
    rm -rf rinetd-0.73/

FROM alpine:3.16.0

COPY --from=builder /build/ /build/

RUN apk add rsync && \
    rsync -av /build/* /usr/ && \
    rm -rf /build/ && \
    apk del rsync

# Check installed version
RUN rinetd -v && \
    mkdir /var/log/rinetd

VOLUME /etc/rinetd
VOLUME /var/log/rinetd

CMD ["rinetd", "-f", "-c", "/etc/rinetd/rinetd.conf"]